import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 3, 50)
fig = plt.figure()

y1 = x ** 2 * np.exp(-(x) ** 2)
y2 = x ** 4 * np.exp(-(x) ** 2)
line1 = plt.plot(x, y1, '--', color="red", label='y1')
line2 = plt.plot(x, y2, 'o-', color="blue", label='y2')

plt.title("Plotting two curves in the same plot")
plt.legend()
plt.xlabel('t')
plt.ylabel('y')
plt.savefig("Picture/fig3.png")
plt.show()

