import numpy as np
import matplotlib.pyplot as plt


def f(x):
    result = x ** 2 * np.exp(-(x) ** 2)
    return result


x = np.linspace(0, 3, 50)
y = f(x)
plt.plot(x, y)
plt.savefig("Picture/fig2.png")
plt.show()
