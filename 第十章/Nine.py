import numpy as np
import matplotlib.pyplot as plt


def H(x):
    if x < 0 or x >= 2:
        return 0
    elif x >= 0 and x <= 1:
        return x
    else:
        return 2 - x


def H1(x):
    r = np.zeros(len(x))  # or r = x.copy()
    for i in range(len(x)):
        r[i] = H(x[i])
    return r


x = np.linspace(-3, 5, 1000)
y = H1(x)

plt.plot(x, y, '-')

plt.title("Plotting hat func in this plot")
plt.savefig("Picture/fig4.png")
plt.show()


