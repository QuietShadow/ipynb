import matplotlib

import matplotlib.pyplot as plt
import numpy as np

# 左图是不同受教育程度育龄妇女生育子女的平均个数统计（按存活子女数统计），
# 右图是不同受教育程度育龄妇女生育子女的存活比例（即存活子女数/活产子女数）；

labels = ['none', 'primary', 'junior', 'senior', 'specialties', 'bachelor', 'master']  # 标签
womenCount = [2052380, 11315444, 20435242, 7456627, 3014264, 1972395, 185028]
birthMen = [2795259, 12698141, 13982478, 2887164, 903910, 432333, 35915]
birthWomen = [2417485, 11000637, 11897674, 2493829, 786862, 385718, 32270]
liveMen = [2717613, 12477914, 13847346, 2863706, 897607, 429809, 35704]
liveWomen = [2362007, 10854232, 11815939, 2480362, 783225, 384158, 32136]

# live = [5079620, 23332146, 25663285, 5344068, 1680832, 813967, 67840]

avglive = [2.47, 2.06, 1.26, 0.72, 0.56, 0.41, 0.37]
avglive2 = [97.45, 98.45, 99.16, 99.31, 99.41, 99.50, 99.49]
plt.figure(figsize=[14, 5])
plt.subplot(121)
plt.plot(labels, avglive, color="red")
ax2 = plt.subplot(122)
plt.plot(labels, avglive2, color="blue")
plt.savefig("Picture/fig8.png")
plt.show()
