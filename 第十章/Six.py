import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq


# 输出最后的结果
def solution():
    "求解的曲线是:"
    print("y=3x^2+2x+1")

    plt.figure(figsize=(8, 6))

    # 画拟合直线
    x = np.linspace(0, 50, 100)  ##在0-15直接画100个连续点
    y = 3 * x * x + 2 * x + 1  ##函数式
    plt.plot(x, y, '--')
    plt.legend()  # 绘制图例
    plt.savefig("Picture/fig1.png")
    plt.show()



solution()
