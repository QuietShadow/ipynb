import matplotlib.pyplot as plt

labels = ['none', 'primary', 'junior', 'senior', 'specialties', 'bachelor', 'master']  # 标签列表
sizes = [2052380, 11315444, 20435242, 7456627, 3014264, 1972395, 185028]  # 绘制数据
colors = ['red', 'orange', 'yellow', 'green', 'purple', 'blue', 'black']
explode = (0, 0, 0.1, 0, 0, 0, 0)  # 只突出第三个切块，偏移比例为0.1 (i.e. 'Hogs')

plt.pie(sizes, explode=explode, labels=labels, colors=colors,
        shadow=True)  # shadow表示添加阴影，startangle表示旋转角度
plt.axis('equal')  # 用于显示为一个长宽相等的饼图
plt.savefig("Picture/fig7.png")
plt.show()  # 展示图像

