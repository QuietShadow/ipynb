# 1、导入模块：
import matplotlib.pyplot as plt

# 2、数据：xstring为年份字符串，ystring为每年评价价格字符串
xstring = '2015 2014 2013 2012 2011 2010 2009 2008 2007 2006 2005 2004 2003 2002 2001 2000'
ystring = '12914 11826 12997 12306.41 12327.28 11406 10608 8378 8667.02 8052.78 6922.52 5744 4196 4336 4588 4751'

# 请在此添加实现代码  根据上述字符串计算x轴和y轴数据  实现柱状图显示 #（参考左侧编程要求中的提示完成）
# ********** Begin *********#
# 1.将数据转换成列表
xvalues = xstring.split(" ")
xvalues.reverse()
yvalues = [float(x) for x in ystring.split(" ")]
yvalues.reverse()
index = [i for i in range(len(xvalues))]
plt.xticks(index, xvalues, rotation=45)
plt.yticks(range(4000, 13500, 1000))
plt.ylim(4000, 13500)
plt.bar(index, yvalues, color='#800080')
# ********** End **********#
plt.savefig("Picture/fig5.png")
plt.show()
