import xml.etree.ElementTree as ET

class InfoManager:
    doc = None
    root = None
    def LoadInfo(self):
        #********** Begin *********#
        self.doc=ET.parse("g:/学习/Python/代码/第八章/ElementTree解析xml/data.xml")

        #********** End **********#
        
    def GetInfoCount(self):
        #********** Begin *********#
        self.root=self.doc.getroot()
        t=self.root.findall("info")
        return len(t)

        #********** End **********#   
            
    def GetAge(self,name):
        #********** Begin *********#
        for s in self.root.findall("info"):
            t=s.get("name")
            if t==name:
                return int(s.get("age"))
        return 0
        #********** End **********#
        
    def GetDescription(self,name):
        #********** Begin *********#
        for s in self.root.findall("info"):
            t=s.get("name")
            if t==name:
                return s.text
        return ""

if __name__=="__main__":
    im= InfoManager()
    im.LoadInfo()
    count=im.GetInfoCount()
    print("共有%s人"%count)
    print("赵昊%s岁，他说%s："%(im.GetAge("赵昊"),im.GetDescription("赵昊")))
    print("龙傲天%s岁，他说%s："%(im.GetAge("龙傲天"),im.GetDescription("龙傲天")))
    print("玛丽苏%s岁，他说%s："%(im.GetAge("玛丽苏"),im.GetDescription("玛丽苏")))
    print("叶良辰%s岁，他说%s："%(im.GetAge("叶良辰"),im.GetDescription("叶良辰")))

