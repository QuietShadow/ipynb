import json


def func():
    file=open("g:/学习/Python/代码/第八章/使用json库/2017.txt","r",encoding="utf-8")
    obj=json.load(file)
    file.close()
    obj_new={"name":"叶良辰","age":17,"height":1.87,"sex":"男性"}
    list1=obj["infos"]
    list1.append(obj_new)
    #print(list1)
    obj["infos"]=list1
    obj["count"]=4
    output = open("g:/学习/Python/代码/第八章/使用json库/2018.txt","w",encoding = "utf-8")
    json.dump(obj,output)
    output.close()

if __name__=="__main__":
    func()
    f = open("g:/学习/Python/代码/第八章/使用json库/2018.txt","r",encoding = "utf-8")
    d = json.load(f)
    print("学生数：%d" % d['count'])
    for s in range(d['count']):
        st = d['infos'][s]
        print("名称：%s，年龄：%d，身高:%.2f，%s" % (st["name"],st["age"],st["height"],st["sex"]))
    f.close()
